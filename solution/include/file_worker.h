//
// Created by Екатерина Пегушина on 24.12.2022.
//

#ifndef IMAGE_TRANSFORMER_FILE_WORKER_H
#define IMAGE_TRANSFORMER_FILE_WORKER_H

#include <stdio.h>

FILE *open_file(char *filename, char *read_code);

#endif //IMAGE_TRANSFORMER_FILE_WORKER_H
