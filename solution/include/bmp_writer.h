//
// Created by Екатерина Пегушина on 24.12.2022.
//

#ifndef IMAGE_TRANSFORMER_BMP_WRITER_H
#define IMAGE_TRANSFORMER_BMP_WRITER_H

#include "bmp_header.h"
#include "image.h"

#include <inttypes.h>
#include <stdio.h>

#define BMP_FILE_TYPE 19778
#define BMP_SIZE 40
#define BMP_BIT_COUNT 24
#define BMP_PIXELS_PER_METER 2834


/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE *out, struct image const *img);


#endif //IMAGE_TRANSFORMER_BMP_WRITER_H
