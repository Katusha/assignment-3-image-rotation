//
// Created by Екатерина Пегушина on 24.12.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <inttypes.h>

#define PIXEL_STRUCT_SIZE sizeof(struct pixel)
#define PIXEL_FACT_SIZE 4

#define IMAGE_STRUCT_SIZE sizeof(struct image)

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

uint64_t get_padding(struct image const *image);

struct image* set_image_params(struct image *source, uint32_t width, uint32_t height, struct pixel *data);

#endif //IMAGE_TRANSFORMER_IMAGE_H
