//
// Created by Екатерина Пегушина on 24.12.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_WORKER_H
#define IMAGE_TRANSFORMER_IMAGE_WORKER_H

#include "image.h"

#include <stdlib.h>

struct image rotate_image(struct image const *not_rotated_image);

#endif //IMAGE_TRANSFORMER_IMAGE_WORKER_H
