//
// Created by Екатерина Пегушина on 24.12.2022.
//

#ifndef IMAGE_TRANSFORMER_BMP_READER_H
#define IMAGE_TRANSFORMER_BMP_READER_H

#include "image.h"

#include <stdio.h>

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    TROUBLES_DURING_READ_ALLOCATION
};

enum read_status from_bmp(FILE *in, struct image *img);


#endif //IMAGE_TRANSFORMER_BMP_READER_H
