//
// Created by Екатерина Пегушина on 25.12.2022.
//

#include "image_worker.h"

struct image rotate_image(struct image const *not_rotated_image) {

    struct image rotated_image;

    uint64_t new_height = not_rotated_image->width;
    uint64_t new_width = not_rotated_image->height;

    rotated_image.height = new_height;
    rotated_image.width = new_width;

    rotated_image.data = malloc(PIXEL_STRUCT_SIZE * new_height * new_width);

    if (rotated_image.data == NULL) {
        return rotated_image;
    }

    for (uint64_t w_pointer = 0; w_pointer < new_width; w_pointer++) {
        for (uint64_t h_pointer = 0; h_pointer < new_height; h_pointer++) {
            rotated_image.data[new_width + h_pointer * new_width - w_pointer - 1] =
                    not_rotated_image->data[h_pointer + new_height * w_pointer];
        }
    }

    return rotated_image;


}
