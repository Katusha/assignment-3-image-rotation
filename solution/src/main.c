#include <stdio.h>
#include <stdlib.h>

#include "bmp_reader.h"
#include "bmp_writer.h"
#include "file_worker.h"
#include "image.h"
#include "image_worker.h"

int main(int argc, char **argv) {

    if (argc != 3) {

        fprintf(stderr, "Программа ожидает на вход два аргумента!");
        return 1;

    }

    FILE* source_file = open_file(argv[1], "rb");
    FILE* rotated_file = open_file(argv[2], "wb");

    if (!source_file) {
        fprintf(stderr, "Не найден исходный файл!");
        return 2;
    }

    if (!rotated_file) {
        fprintf(stderr, "Не удалось создать/открыть выходной файл!");
        return 2;
    }

    struct image* new_image = malloc(IMAGE_STRUCT_SIZE);

    if (!new_image) {
        fprintf(stderr, "Не удалось аллоцировать память!");
        return 2;
    }

    enum read_status read_status = from_bmp(source_file, new_image);

    if (read_status != READ_OK) {
        fclose(source_file);
        fclose(rotated_file);
        fprintf(stderr, "Не удалось прочитать структуру из файла!");
        return 2;
    }

    struct image rotated_image = rotate_image(new_image);

    if (rotated_image.data == NULL) {
        fprintf(stderr, "Не удалось перевернуть изображение!");
        return 2;
    }

    enum write_status write_status = to_bmp(rotated_file, &rotated_image);

    if (write_status != WRITE_OK) {
        free(new_image->data);
        free(new_image);
        fclose(source_file);
        fclose(rotated_file);
        fprintf(stderr, "Не удалось записать структуру в файл!");
        return 2;
    }

    fclose(source_file);
    fclose(rotated_file);
    free(new_image->data);
    free(new_image);
    free(rotated_image.data);


    return 0;
}
