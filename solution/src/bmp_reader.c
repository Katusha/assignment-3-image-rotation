//
// Created by Екатерина Пегушина on 25.12.2022.
//

#include "bmp_reader.h"

#include <stdio.h>
#include <stdlib.h>

#include "bmp_header.h"


enum read_status from_bmp(FILE *in, struct image *img) {

    if (!in || !img) {
        return READ_INVALID_SIGNATURE;
    }

    struct bmp_header* read_bmp_header = malloc(BMP_STRUCT_SIZE);

    if (!read_bmp_header) {
        return TROUBLES_DURING_READ_ALLOCATION;
    }

    if (!fread(read_bmp_header, BMP_STRUCT_SIZE, 1, in)) {
        return READ_INVALID_HEADER;
    }

    struct pixel *new_img_data = malloc(PIXEL_FACT_SIZE * read_bmp_header->biWidth * read_bmp_header->biHeight);
    if (new_img_data == NULL) {
        return TROUBLES_DURING_READ_ALLOCATION;
    }

    img = set_image_params(img, read_bmp_header->biWidth, read_bmp_header->biHeight, new_img_data);

    fseek(in, read_bmp_header->bOffBits, SEEK_SET);

    uint64_t padding_size = get_padding(img);
    printf("%" PRId64 "\n", padding_size);

    for (uint64_t h_pointer = 0; h_pointer < img->height; h_pointer++) {
        for (uint64_t w_pointer = 0; w_pointer < img->width; w_pointer++) {
            if (!fread(img->data + (w_pointer + h_pointer * img->width), PIXEL_STRUCT_SIZE, 1, in)) {
                free(read_bmp_header);
                free(img->data);
                return READ_INVALID_BITS;
            }
        }
        fseek(in, (int8_t) padding_size, SEEK_CUR);
    }

    free(read_bmp_header);

    return READ_OK;

}
