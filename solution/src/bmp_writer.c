//
// Created by Екатерина Пегушина on 25.12.2022.
//

#include "bmp_writer.h"

#include <stdio.h>

#include "bmp_header.h"

static struct bmp_header construct_bmp_header(struct image const *image) {

    uint64_t padding_size = get_padding(image);

    uint32_t image_size = PIXEL_STRUCT_SIZE * image->height * (image->width + padding_size);
    uint32_t file_size = BMP_STRUCT_SIZE * image_size;

    struct bmp_header constructed_header = {
            .bfType = BMP_FILE_TYPE,
            .biHeight = image->height,
            .biWidth = image->width,
            .biSizeImage = image_size,
            .bfileSize = file_size,
            .bOffBits = BMP_STRUCT_SIZE,
            .biSize = BMP_SIZE,
            .biXPelsPerMeter = BMP_PIXELS_PER_METER,
            .biYPelsPerMeter = BMP_PIXELS_PER_METER,
            .biBitCount = BMP_BIT_COUNT,
            .biPlanes = 0,
            .bfReserved = 0,
            .biCompression = 0,
            .biClrImportant = 0,
            .biClrUsed = 0
    };

    return constructed_header;
}

enum write_status to_bmp(FILE *out, struct image const *img) {

    if (!out || !img) {
        return WRITE_ERROR;
    }

    struct bmp_header constructed_header = construct_bmp_header(img);

    uint64_t padding_size = get_padding(img);

    printf("%" PRId64 "\n", padding_size);

    size_t header_write_status = fwrite(&constructed_header, BMP_STRUCT_SIZE, 1, out);

    if (header_write_status != 1) {
        return WRITE_ERROR;
    }

    int zero_byte_for_padding = 0;

    for (uint64_t h_pointer = 0; h_pointer < img->height; h_pointer++) {
        for (uint64_t w_pointer = 0; w_pointer < img->width; w_pointer++) {
            if (!fwrite(img->data + h_pointer * img->width + w_pointer, PIXEL_STRUCT_SIZE, 1, out)) {
                return WRITE_ERROR;
            }
        }

        if (!fwrite(&zero_byte_for_padding, padding_size, 1, out)) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;


}


