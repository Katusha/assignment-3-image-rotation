//
// Created by Екатерина Пегушина on 23.01.2023.
//

#include "image.h"

uint64_t get_padding(struct image const *image) {
    return (PIXEL_FACT_SIZE - ((image->width * PIXEL_STRUCT_SIZE) % PIXEL_FACT_SIZE));
}

struct image* set_image_params(struct image *source, uint32_t width, uint32_t height, struct pixel *data) {
    source->width = width;
    source->height = height;
    source->data = data;
    return source;
}
